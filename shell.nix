{ sources ? import ./nix/sources.nix }:

let
  overlay = _: pkgs: { niv = import sources.niv { }; };
  pkgs = import sources.nixpkgs {
    overlays = [ overlay ];
    config = { };
  };
  pythonPackages = ps: with ps; [
    jupyterlab
  ];
  pyWithPkgs = pkgs.python3.withPackages pythonPackages;
in pkgs.mkShell {
  buildInputs = [
    pyWithPkgs

    # keep this line if you use bash
    pkgs.bashInteractive
  ];
}
